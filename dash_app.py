import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd

df = pd.read_csv("drill_2019-01-01T07:00:00_2019-12-31T23:59:59.csv")

authors = [{"label": name, "value": name} for name in df.author.unique()]


def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +
        # Body
        [
            html.Tr([html.Td(dataframe.iloc[i][col]) for col in dataframe.columns])
            for i in range(min(len(dataframe), max_rows))
        ],
        id="my-table",
    )


external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(
    children=[
        html.H4(children="Git modifications"),
        dcc.Dropdown(id="my-dropdown", options=authors, value=authors[0]["value"]),
        generate_table(df),
    ]
)


@app.callback(Output("my-table", "children"), [Input("my-dropdown", "value")])
def update_graph(selected_dropdown_value):
    df = pd.read_csv("drill_2019-01-01T07:00:00_2019-12-31T23:59:59.csv")
    max_rows = 10

    df = df[df.author == selected_dropdown_value]
    return [html.Tr([html.Th(col) for col in df.columns])] + [
        html.Tr([html.Td(df.iloc[i][col]) for col in df.columns])
        for i in range(min(len(df), max_rows))
    ]


if __name__ == "__main__":
    app.run_server(debug=True)
