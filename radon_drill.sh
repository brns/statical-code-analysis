#!/bin/bash
set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

repo="${1:-https://github.com/brnsnt/radon.git}"
echo "Generate Report for: $repo , $DIR"
data_dir="${DIR}/data"
[ -d repo ] || git clone "${repo}" repo
[ -d data ] || mkdir data

cd repo
git pull

IGNORE="BDDPython"
EXCLUDE="*.po,*test*,__init__.py"

# Cyclomatic Complexity
radon cc -i "${IGNORE}" -e  "${EXCLUDE}" -o "SCORE" . -j > ${data_dir}/cc_stats.json
radon cgc -i "${IGNORE}" -e  "${EXCLUDE}" -o "SCORE" . -j > ${data_dir}/cgc_stats.json

# Maintainability index
radon mi  -j -i "${IGNORE}" -e "${EXCLUDE}" -s . > ${data_dir}/mi_stats.json

radon raw -j -i "${IGNORE}" -e "${EXCLUDE}" . > ${data_dir}/raw_stats.json

# Halstead complexity metrics
radon hal -j -f -i "${IGNORE}" -e "${EXCLUDE}" . > ${data_dir}/hal_stats.json

radon hal -j -f -i "${IGNORE}" -e "${EXCLUDE}" . > ${data_dir}/hal_file_stats.json

cd ..
[ -d public ] || mkdir public
# Generate HTML
jupyter nbconvert --execute --to html Radon.ipynb --TemplateExporter.exclude_input=True --output-dir=public

jupyter nbconvert --execute --to html driller.ipynb --TemplateExporter.exclude_input=True --output-dir=public --output=index

