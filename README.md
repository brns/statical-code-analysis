# Statical Code Analysis 

This repository is a work in progress to try out tools to gather code metrics,
analyse and visualise them. 

Jupyter Notebooks are used and exported as Gitlab Pages. 

## Code Metrics

### Radon 

- [Radon Documentation](https://radon.readthedocs.io/en/latest/intro.html)
- [Radon Github](https://github.com/rubik/radon)

    Radon is a Python tool that computes various metrics from the source code. Radon can compute:

- **McCabe's complexity**, i.e. cyclomatic complexity 
- **raw metrics** (these include SLOC, comment lines, blank lines, &c.)
- **Halstead metrics** (all of them)
- **Maintainability Index** (the one used in Visual Studio)

 Additionally this repository uses as well cognitive complexity (experimental implementation https://github.com/brnsnt/radon/tree/cognitive-complexity)

 [Radon.ipynb](https://gitlab.gonnado.com/bruno/git-inspector/blob/master/Radon.ipynb)
 
## Git History

Create a solution to visualize and measure our git contributions.

For some ideas what could be visualised from a git repository see:

- [gitinspector](https://github.com/ejwa/gitinspector)
- [Gitlab Contribution Graphs](https://gitlab.gonnado.com/gonnado/gonnado/-/graphs/master)
- [Gitlab Commit Charts](https://gitlab.gonnado.com/gonnado/gonnado/-/graphs/master/charts)

Metrics can easily be gathered with [PyDriller](https://github.com/ishepard/pydriller).
The following notebook and module provide some examples and information
on how to use pydriller. 

It uses [Plotly](https://plot.ly/python/) as graph library for some quick graphs. 


- [driller.ipynb](https://gitlab.gonnado.com/bruno/git-inspector/blob/master/driller.ipynb?expanded=true&viewer=rich)
- [driller.py](https://gitlab.gonnado.com/bruno/git-inspector/blob/master/driller.py) 


-------------------------------------------


## Technology


### Dash App 

Dash is a web application framework for data visualization apps built with Flask, Plotly.js and React.js.
Apps can be completely written in Python and the whole JavaScript gets generated. 
It combines some of the currently best technologies for data visualization 
and claims to be easy to use. 


`dash_app.py` provides a minimal example for a [Dash Application](https://dash.plot.ly/).
It simply shows the first 10 commits in a table with a dropdown to filter by author. 


``` shell
pip install dash
python dash_app.py
```




-----------------------------------------------

## Usage 

### Installation

To install a notebook kernel in a virtualenv execute the following and select the kernel in the notebook.

    #..activate venv

    pip install pydriller plotly pandas ipython ipykernel jupyter chart_studio radon
    
    # activate venv again (ipython must be in venv)
    
    ipython kernel install --user --name=.pydriller

    jupiter notebook
    
Make sure to adjust the path to the git repository!
    

### Generate Radon Statistics

Radon can also be used as python module, but this does not work in Jupoyter Notebooks. 
Thus the stats are stored as json using the `radon_dril.sh` script. 

``` shell
./radon_drill.sh
```


### References and Dependencies

- [itables](https://github.com/mwouts/itables)


