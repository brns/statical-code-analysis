import os.path
from datetime import datetime

import pandas as pd
from pydriller import RepositoryMining

import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import math

from radon.cli.harvest import RawHarvester
from radon.complexity import cc_rank, SCORE
from radon.cli import Config

#r = RawHarvester().run()
#r.as_json()

from radon.cli.harvest import CCHarvester

def radon_harvester_example():
    path = "driller.py"

    config = Config(
        exclude="*.ipynb",
        ignore="",
        order=SCORE,
        no_assert=True,
        show_closures=False,
        include_ipynb=True,
        ipynb_cells=True,
        min="A",
        max="F",
    )
    h = CCHarvester(path, config)
    #next(h.results)



def get_modifications(since: datetime, to: datetime, repo_path: str):
    """
    Collect all modifications to the master branch from the repository.
    """
    for commit in [
        c
        for c in RepositoryMining(repo_path, since=since, to=to).traverse_commits()
        if c.in_main_branch and not c.merge
    ]:
        for mod in commit.modifications:
            if mod.change_type == mod.change_type.MODIFY:
                yield (
                    commit.author.name,
                    commit.author_date,
                    commit.hash,
                    commit.msg,
                    mod.old_path,
                    mod.filename,
                    mod.complexity,
                    mod.nloc,
                )
            elif mod.change_type == mod.change_type.DELETE and False:
                yield (
                    commit.author.name,
                    commit.author_date,
                    commit.hash,
                    commit.msg,
                    mod.old_path,
                    mod.filename,
                    0,
                    0,
                )


def get_modification_data(repo_path: str, since: datetime = None, to: datetime = None):
    """Load the data for the time-range from the csv
    or by mining the repository."""
    since = since or datetime(2019, 1, 1, 7, 0, 0)
    to = to or datetime.today()

    fn = "drill_{}_{}.csv".format(since.isoformat(), to.isoformat())
    if os.path.isfile(fn):
        df = pd.read_csv(fn)
    else:
        df = pd.DataFrame(
            get_modifications(since, to, repo_path),
            columns=[
                "author",
                "date",
                "hash",
                "msg",
                "path",
                "filename",
                "cyclo",
                "nloc",
            ],
        )
        df.to_csv(fn, index=False)
    return df


# Radon
from radon.cli.harvest import RawHarvester
from radon.complexity import cc_rank, SCORE
from radon.cli import Config


from radon.cli.harvest import CCHarvester


def radon_harvest_cc(path, ignore="", exclude=""):
    """Does not work with the notebook because ipython messes up the os path..."""

    config = Config(
        exclude=ignore,
        ignore=exclude,
        order=SCORE,
        no_assert=True,
        show_closures=False,
        include_ipynb=True,
        ipynb_cells=True,
        min="A",
        max="F",
    )
    h = CCHarvester(path, config)
    return h.results


def cc_bubbleplot(df):
    """https://plot.ly/python/bubble-charts/"""
    hover_text = []
    bubble_size = []

    for index, row in df.iterrows():
        hover_text.append(
            (
                "Name: {name}<br>"
                + "File: {_file}<br>"
                + "App: {app}<br>"
                + "Complexity: {complexity}<br>"
                + "Size: {size}<br>"
            ).format(
                name=row["name"],
                _file=row["file"],
                app=row["module"],
                complexity=row["cognitive"],
                size=row["size"],
            )
        )
        bubble_size.append(math.sqrt(row["cognitive"]))

    df["text"] = hover_text
    df["bubble_size"] = bubble_size
    sizeref = 2.0 * max(df["size"]) / (100 ** 2)

    # Dictionary with dataframes for each app
    app_names = df.module.unique()
    app_data = {app: df.query("module == '%s'" % app) for app in app_names}

    # Create figure
    fig = go.Figure()

    for app_name, app in app_data.items():
        fig.add_trace(
            go.Scatter(
                x=app["size"],
                y=app["cognitive"],
                name=app_name,
                text=app["text"],
                marker_size=app["size"],
            )
        )

        # Tune marker appearance and layout

    fig.update_traces(
        mode="markers", marker=dict(sizemode="area", sizeref=sizeref, line_width=2)
    )

    fig.update_layout(
        title="Methods and Functions: Complexity / Size",
        xaxis=dict(
            title="Total Size (incl. comments)", gridcolor="white", gridwidth=2,
        ),
        yaxis=dict(title="Cyclomatic Complexity", gridcolor="white", gridwidth=2,),
        paper_bgcolor="rgb(243, 243, 243)",
        plot_bgcolor="rgb(243, 243, 243)",
    )

    return fig
